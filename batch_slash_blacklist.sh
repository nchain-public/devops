#!/bin/bash
mpush='amcli -u http://120.77.177.218 push action'
mget='amcli -u http://120.77.177.218 get account'
um='amcli wallet unlock -n amax-core --password $pwd'

input=$1
pwd=$2
start=$3

[ -z "$start" ] && start=0

$um


marks=( '/' '-' '\' '|' )
i=0
while IFS= read -r line
do
  i=$((i+1))
  [ "$i" -le "$start" ] && continue

  IFS=', ' read -r -a array <<< $line
  acct="${array[0]}"

  let j=$(($i % 4))
  quant=$($mget $acct |grep liquid|cut -d ':' -f2 | xargs)
  echo -ne "  :: $acct $quant [$i] ${marks[j  % ${#marks[@]}]} ::\r";
  [ "$quant" != "0.00000000 AMAX" ] && $mpush amax.token slashblack "[\"$acct\",\"$quant\",\"DeGov slash\"]" -pamax > /dev/null 2>&1 && echo -ne slashed

  let m=$(($i % 3000))
  [ $m -eq 0 ] && $um

done < "$input"
