#!/bin/bash

export  RICHARD_CHEN_SSH_PUB_KEY="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCbaiqTNAXoSKqMQGy3S7QP1WtSrNGlDtVlGO1EDu0QhcJAMeXP6m9RRy8PUUiwXtQXFhpm6CK4IykYb48HRIaoQa93h7z9W8wZirWq2C3dpfzMxy5Mf6ErdfMs9mIWiTTG+18wUAoEy5zC3IgOUu1XOesvq+a3D2JQIxZsYDXkoEY06kmQLNola51UQ1VPSjjXZ0wvlKeo3UpnBTQ723v3Hy3R7f7TsOwY/UkEAXIlMqxoerzcTaHW8pti/8pM9m3x4Gd4kWw7APcmySoqbESTraQBRWzsUN1POujpE345Yyob4TLxYXDMPizJgXSAwveJolGmHk5iF54+UOTgwmf8fYckXX08WaHk0oorANbrT4K5g3/RaT/JjeLkr5DzgP23gLYbMCaPEIcsN+86JW3t9yfwpVdxOLvmcoD5z91Tdzp/U1RAm5A7lvu1KkvhwAl7y7zDGO6ZKjYYNWNk1fcpR9zi2oiOfGUKWxTyCAK9Gnr+u1g1zLxbB+IOq5v6IoX32aGaQPeXcSxklg2hxWJbNhui9bKVcvKRHhiD8ko2sSYjWv4Q7Q2qclqljAtn1cdeaJXB2x357JDGEoyMNiH4PG4iapXRz9PojVCqwbLufgAwt52JCd/scVPVcSW2YyQvy5aiH9E54UQx5mB0NtMM4clOczm8JFc4GrI/iP9s9w== richardchen@RC-MBP21.local"


#添加 sudo 用户组
sudo_add(){
	gptag=`grep sudo /etc/group`
	gdtag=`grep 980  /etc/group`

	if [ x$gptag == "x" ];then
		if [ x$gdtag == "x" ];then
			groupadd -g 980 sudo
			echo  "%sudo  ALL=(ALL)  NOPASSWD: ALL" > /etc/sudoers.d/sudo_group
		fi
	fi
}

#添加 docker 用户组
docker_add(){
	gptag=`grep docker /etc/group`
	gdtag=`grep 990  /etc/group`

	if [ x$gptag == "x" ];then
		if [ x$gdtag == "x" ];then
			groupadd -g 990 docker
		fi
	fi
}


#安装 zsh shell
init_zsh(){
	 yum install zsh git -y
	 su -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)" - richardchen
	 su -c "$(cp /tmp/mytheme.zsh-theme /home/richardchen/.oh-my-zsh/themes)" - richardchen
	 su -c "$(cp -f /tmp/.zshrc /home/richardchen)" - richardchen
}


#添加 richard 用户
user_add(){
		useradd -s /bin/zsh richardchen
		usermod  richardchen  -aG  sudo
		usermod  richardchen  -aG  docker
		
		useradd -s /bin/zsh devops
		usermod  devops  -aG  sudo
		usermod  devops  -aG  docker
}

#添加 richard 公钥
pubkey_add(){
	mkdir  -p  /home/richardchen/.ssh
	ssh-keygen -t rsa -P '' -f  /home/richardchen/.ssh/id_rsa
	touch  /home/richardchen/.ssh/authorized_keys
	echo $RICHARD_CHEN_SSH_PUB_KEY > /home/richardchen/.ssh/authorized_keys
	chown -R  richardchen.richardchen  /home/richardchen/.ssh
	chmod 640  /home/richardchen/.ssh/authorized_keys
	chmod 600  /home/richardchen/.ssh/id_rsa
}


sudo_add
docker_add
user_add
pubkey_add
init_zsh
