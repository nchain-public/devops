#!/bin/bash
#
# 初始化系统脚本
#
# 适用centos6 和centos7
#
#


PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH
cur_dir=$(pwd)
gcc_version=`rpm -qa gcc | awk -F '[-]' '{print $2}'`

VERSION=`cat /etc/issue | grep '6.'`
if [ "$VERSION" == "" ];then
    VERSION='centos7'
else
    VERSION='centos6'
fi

#更改为阿里云的源

# yum install  epel-release  wget -y
# mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
# if [ $VERSION == 'centos7' ];then
#    wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
#else
#    wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-6.repo
#fi
#yum clean all
#yum makecache

mkdir -p /opt/docker-instances

#更改ulimit参数
if [ "`cat /etc/security/limits.conf | grep 'soft nproc 65535'`" = "" ]; then
cat  >> /etc/security/limits.conf << EOF
* soft nproc 65535
* hard nproc 65535
* soft nofile 65535
* hard nofile 65535
EOF
echo "ulimit -SHn 65535" >> /etc/profile
echo "ulimit -SHn 65535" >> /etc/rc.local
fi

#安装必要工具
yum update -y
yum install -y vim ntpdate sysstat wget man mtr lsof iotop iostat nscd net-tools  

#"安装系统工具"
yum install -y gcc gcc-c++ make cmake autoconf bzip2 bzip2-devel curl \
 openssl openssl-devel openssl-perl rsync gd zip perl unzip zip  htop 


#关闭selinux
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
setenforce 0

#设置ssh  Port 9888
sed -i "s/#Port 22/Port 19888/g" /etc/ssh/sshd_config
sed -i "s/UseDNS yes/UseDNS no/g" /etc/ssh/sshd_config
sed -i "s/Allowusers root ultraserve/AllowUsers root richardchen devops/g" /etc/ssh/sshd_config
sed -i "s/GSSAPIAuthentication yes/GSSAPIAuthentication no/g" /etc/ssh/sshd_config

#设置时区
if [ "`cat /etc/crontab | grep ntpdate`" = "" ]; then
echo "0 23 * * * root /usr/sbin/ntpdate cn.pool.ntp.org >> /var/log/ntpdate.log" >> /etc/crontab
fi
rm -f /etc/localtime
ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
ntpdate cn.pool.ntp.org;hwclock -w


# Del unnecessary users 删除不必要的用户
echo "Del unnecessary users."
for USERS in adm lp sync shutdown halt mail news uucp Operator games gopher
do
    grep $USERS /etc/passwd &>/dev/null
    if [ $? -eq 0 ]
    then
        userdel $USERS &> /dev/null
    fi
done


# Del unnecessary groups 删除不必要的用户组
echo "Del unnecessary groups."
for GRP in  lp news uucp games gopher null floppy dip pppusers popusers slipusers 
do
    grep $GRP /etc/group &> /dev/null
    if [ $? -eq 0 ]
    then
        groupdel $GRP &> /dev/null
    fi
done


# Disabled reboot by keys ctlaltdelete 禁用ctlaltdelete重启功能
echo "Disabled reboot by keys ctlaltdelete"
sed -i 's/^exec/#exec/' /etc/init/control-alt-delete.conf


#Set history commands  设置命令历史记录参数
echo "Set history commands."
sed -i 's/HISTSIZE=1000/HISTSIZE=100/' /etc/profile
sed -i "8 s/^/alias vi='vim'/" /root/.bashrc

grep 'HISTFILESIZE' /etc/bashrc &>/dev/null 
if [ $? -ne 0 ] 
then
cat << EOF >> /etc/bashrc
HISTFILESIZE=4000
HISTSIZE=4000
HISTTIMEFORMAT='%F/%T'
EOF
fi
source /etc/bashrc

#安装 iptables-services 
iptsvs_install(){
    yum install iptables-services -y
    ln -s  /usr/libexec/iptables/iptables.init  /etc/init.d/iptables
}

#设置 iptables 开放端口
iptables_set(){

iptables -P INPUT ACCEPT  
iptables -A INPUT -i lo -j ACCEPT
iptables -A INPUT -p tcp --dport 22 -j ACCEPT  
iptables -A INPUT -p tcp --dport 19777 -j ACCEPT  
iptables -A INPUT -p tcp --dport 19888 -j ACCEPT
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT  
iptables -P INPUT DROP  
iptables -P OUTPUT ACCEPT  
iptables -P FORWARD ACCEPT 

/etc/init.d/iptables  save
/etc/init.d/iptables  restart
/etc/init.d/iptables  status

}


#设置sysctl
SYSCONF="
#Add
net.ipv4.tcp_max_syn_backlog = 65536
net.core.netdev_max_backlog =  32768
net.core.somaxconn = 32768

net.core.wmem_default = 8388608
net.core.rmem_default = 8388608
net.core.rmem_max = 16777216
net.core.wmem_max = 16777216
net.ipv4.tcp_timestamps = 0
net.ipv4.tcp_synack_retries = 2
net.ipv4.tcp_syn_retries = 2

net.ipv4.tcp_tw_recycle = 1
#net.ipv4.tcp_tw_len = 1
net.ipv4.tcp_tw_reuse = 1

net.ipv4.tcp_mem = 94500000 915000000 927000000
net.ipv4.tcp_max_orphans = 3276800

net.ipv4.tcp_fin_timeout = 120
net.ipv4.tcp_keepalive_time = 120
net.ipv4.ip_local_port_range = 1024  65535

net.nf_conntrack_max = 16404388
net.netfilter.nf_conntrack_tcp_timeout_established = 10800

#kernel: TCP: time wait bucket table overflow
net.ipv4.tcp_max_tw_buckets = 30000

fs.file-max=655350"


#重启服务
#ssh
if [ "$VERSION" == "centos6" ]; then
    service sshd restart
    iptables_set
    if [ "`cat /etc/sysctl.conf | grep net.ipv4.tcp_max_tw_buckets`" = "" ]; then
        echo "$SYSCONF" >> /etc/sysctl.conf    
    fi
    /sbin/sysctl -p
else
    systemctl restart sshd
    systemclt disable postfix.service
    systemctl stop postfix.service
    systemctl stop firewalld
    systemctl mask firewalld
    iptsvs_install
    iptables_set
    if [ ! -f '/etc/sysctl.d/sys_ext.conf' ];then
        echo "$SYSCONF" >> /etc/sysctl.d/sys_ext.conf 
    fi 
    /sbin/sysctl -p
fi


if [[ -n $1 ]]; then
    # hostname set
    if [ "$VERSION" == "centos6" ]; then
    	if [ x${1} != "x" ]; then
    		hostname  $1
    		echo  "HOSTNAME=${1}" >> /etc/sysconfig/network
    	fi
    else
    	if [ x${1} != "x" ]; then
                    hostnamectl set-hostname $1
                    echo  "HOSTNAME=${1}" >> /etc/sysconfig/network
            fi
    fi
fi

sudo yum install rpcbind
sudo systemctl disable rpcbind
sudo systemctl disable rpcbind.socket
sudo systemctl stop rpcbind

## update security
sudo yum update kernel
sudo yum update kernel-devel
sudo yum update kernel-headers
sudo yum update kernel-tools
sudo yum update kernel-tools-libs
sudo yum update python-perf
sudo yum update kernel-firmware
sudo yum update kernel-headers

sudo yum update nss-util
sudo yum update nss
sudo yum update nss-sysinit
sudo yum update nss-tools

sudo yum update bind-libs
sudo yum update bind-utils

sudo yum update ntp
sudo yum update ntpdate
