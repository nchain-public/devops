#!/bin/bash
#
#

if [ $# -lt 1 ];then
    echo "ERROR! Usage: $(basename $0) DOMAIN"
    exit 1
fi

domain=$1
pem_path=/home/devops/bin/ssl/$domain.pem
key_path=/home/devops/bin/ssl/$domain.key
curl -X POST  http://172.17.0.1:8001/certificates/   -H 'Content-Type: application/json'   -d "{\"cert\": \"$(cat $pem_path)\", \"key\": \"$(cat $key_path)\", \"snis\": [ \"$domain\" ]}"