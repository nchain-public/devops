alias rm='rm -i'
alias l='ls -la'
alias c='clear'
alias dex='sh /data/sbin/dex.sh'
alias dps='docker ps'
alias dpsa='docker ps -a'
alias dimg='docker images'
alias sc='systemctl'
alias dex='docker exec -it $1'

alias cl='dex bps cleos'

alias  dimg='docker images'
alias  dps='docker ps'
alias  dpsa='docker ps -a'
alias  drm='docker rm'
alias  drmi='docker rmi'
alias  dex='docker exec -it '
alias  dstart='docker start'
alias  dstop='docker stop'
alias  drestart='docker restart'
alias  dstats='docker stats --no-stream'
alias  dlogs='docker logs'
alias  dtag='docker tag'
alias  drenm='docker rename'
alias  dpull='docker pull'
alias  dpush='docker push'
alias  dsch='docker search'
alias tailf='tail -f'


export HISTTIMEFORMAT="%Y%m%d-%H%M%S: "
alias pssh="pssh -P -h /usr/src/host"
alias pscp="pscp  -h /usr/src/host"
alias vi=vim
alias scl=systemctl
alias py=python
alias py3=python3
alias ls='ls --time-style=long-iso --color=auto'
alias cp="cp --backup=t -av"
alias mv="mv --backup=t -v"
alias pwd="pwd -P"
alias tree="tree -CF"
alias grep='egrep -i --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'
alias mkdir="mkdir -pv"
alias du="du -h"
alias ipy="ipython"
alias c="clear"
alias sst="ss -anptl | column -t"
alias ssu="ss -anupl | column -t"
alias taf="tail -f"
alias dc="docker-compose"
alias dce="docker-compose exec"
alias dcd="docker-compose down"
alias dcu="docker-compose up -d"
alias mount='mount |column -t'
alias h='history'
alias ping='ping -c 4 -i.2'
alias wget="wget -c"
alias iptlist="iptables -L -n --line-number | column -t"
alias iptin="iptables -L INPUT -n --line-number | column -t"
alias iptout="iptables -L OUPUT -n --line-number | column -t"
alias iptfw="iptables -L FORWARD -n --line-number | column -t"
alias ipe='curl ipinfo.io/ip'
alias addr="ip -4 addr"
alias www='python -m SimpleHTTPServer 8000'
alias untar='tar -xf'
alias df='df -h | egrep -v "tmpfs|overlay2|containers" | column -t'
alias stl='supervisorctl'
alias makemigrations="python3.6 manage.py makemigrations"
alias migrate="python3.6 manage.py migrate"


#################################################################
# kubernetes commands shortcuts
alias k="kubectl"
alias nodes="kubectl get nodes -o wide"
alias allpods="kubectl get pods --all-namespaces -o wide"
alias pods="kubectl get pods -o wide "
alias kds="kubectl describe "
alias rss="kubectl get rs -o wide "
alias dss="kubectl get ds -o wide "
alias sas="kubectl get sa -o wide "
alias roles="kubectl get roles -o wide"
alias nss="kubectl get ns -o wide"
alias pvs="kubectl get pv -o wide "
alias pvcs="kubectl get pvc -o wide"
alias svcs="kubectl get svc -o wide "
alias cms="kubectl get cm -o wide "
alias sts="kubectl get sts -o wide "
alias kexec="kubectl exec -it "
alias kde="kubectl delete"
alias ked="kubectl edit "
alias klo="kubectl logs "
alias kplain="kubectl explain "
alias kce="kubectl create "
alias kapply="kubectl apply"
alias dps="kubectl get deploy -o wide "
alias scs="kubectl get secrets "
alias kslo="kubectl logs --since=3s "
alias certs="kubectl get certificate "
alias crds="kubectl get crd"
alias igs="kubectl get ingress "
alias gts="kubectl get Gateways"
alias dts="kubectl get DestinationRules"
alias vss="kubectl get VirtualServices"
alias eps="kubectl get endpoints"
#################################################################

alias ddu="ls -F | grep '/$' | xargs -i du -s {} | sort -rn | cut -f2 | xargs -i du -sh {}"
alias fdu="ls -F | grep -v '/$' | xargs -i du -s {} | sort -rn | cut -f2 | xargs -i du -sh {}"
source <(kubectl completion zsh)
